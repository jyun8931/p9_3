import java.time.LocalDate;

public class Monthly extends Appointment{
    public Monthly(String description, LocalDate date){
        super(description, date);
    }

    @Override
    public Boolean occursOn(int year, int month, int day) {
        if(getLocDate().getDayOfMonth() == day && getLocDate().isAfter(LocalDate.of(year, month, day)))
            return true;
        else
            return false;

    }
}

import java.sql.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class P9_3 {
    public static void checkAppointment(ArrayList<Appointment> apps) {
        int iYear, iMonth, iDay;
        Scanner keyboard = new Scanner(System.in);

        System.out.printf("Enter a year: ");
        iYear = keyboard.nextInt();

        System.out.printf("Enter a Month: ");
        iMonth = keyboard.nextInt();

        System.out.printf("Enter a Day: ");
        iDay = keyboard.nextInt();

        System.out.println("Here is a List of appointments after the date you have entered");
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).occursOn(iYear, iMonth, iDay))
                System.out.printf("You have a %s appointment on %s\n", apps.get(i).getStrDescription(), apps.get(i).getLocDate().toString());
        }
    }

    public static void addAppointment(ArrayList <Appointment> myArray){
        System.out.print("Enter the type (O - Onetime, D - Daily, or M - Monthly): ");
        Scanner input = new Scanner(System.in);
        String type = input.next();
        String description;
        String date;
        LocalDate sometime;
        switch (type) {
            case "O" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Onetime someAppointment = new Onetime(description, sometime);
                myArray.add(someAppointment);
            }
            case "D" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Daily anotherAppointment = new Daily(description, sometime);
                myArray.add(anotherAppointment);
            }
            case "M" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Monthly anAppointment = new Monthly(description, sometime);
                myArray.add(anAppointment);
            }
        }
    }

    public static void main(String[] args) {
        ArrayList <Appointment> appList = new ArrayList<Appointment>();
        LocalDate dentist = LocalDate.of(2020, 11, 14);
        LocalDate schoolLecture = LocalDate.of(2020, 8, 26);
        LocalDate studentBodyMeeting = LocalDate.of(2021, 1, 4);
        Appointment appDentist = new Onetime("dentist", dentist);
        Appointment appSchool =  new Daily("School Lecture", schoolLecture);
        Appointment appStudentMeeting = new Monthly("Student Body Meeting", studentBodyMeeting);
        //given list of appointments
        appList.add(appDentist);
        appList.add(appSchool);
        appList.add(appStudentMeeting);

        Scanner user = new Scanner(System.in);

        String choice;

        do{
            System.out.printf("Select a option: A for add an appointment, C for checking, Q to quit: ");
            choice = user.next();
            if(choice.equals("C"))
                checkAppointment(appList);
            else if(choice.equals("A"))
               addAppointment(appList);

        }while(choice.compareTo("Q") != 0);

    }
}

import java.time.LocalDate;

public class Daily extends Appointment{
    public Daily(String description, LocalDate date){
        super(description, date);
    }
    public Boolean occursOn(int year, int month, int day){
        if(this.getLocDate().isAfter(LocalDate.of(year, month, day)))
            return true;
        return false;
    }

}
